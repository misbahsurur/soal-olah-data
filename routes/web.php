<?php

use App\Http\Controllers\ListController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ListController@index');
Route::get('/produk',[ListController::class, 'ListProduk']);
Route::get('/customer',[ListController::class, 'ListCustomer']);
Route::get('/agent',[ListController::class, 'ListAgent']);
Route::get('/show/{id}',[ListController::class, 'show']);
Route::get('/get_ajax_data',[ListController::class, 'get_ajax_data']);
Route::get('/daterange',[ListController::class,'daterange']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
