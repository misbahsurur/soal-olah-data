<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Soal Olah Data</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h1>Halaman Order</h1>
                <button class="btn btn-success" onClick="produk()">List Produk</button>
                <button class="btn btn-warning" onClick="customer()">List Customer</button>
                <button class="btn btn-danger" onClick="agent()">LIst Agent</button>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="serach" id="serach" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <form action="{{ url('daterange') }}" method="GET">
                    <div class="col-md-5">
                        <div class="input-group input-daterange">
                            <input type="date" name="from_date" id="from_date" class="form-control" />
                            <div class="input-group-addon">to</div>
                            <input type="date"  name="to_date" id="to_date" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" name="filter" id="filter" class="btn btn-info btn-sm">Filter</button>
                        <button onClick="refresh()" class="btn btn-warning btn-sm">Refresh</button>
                    </div>
                    </form>
                </div>
                <div id="read" class="mt-3"></div>
                <div id="table_data">
                    @include('order_data')
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="page" class="p-2"></div>
                </div>
            </div>
        </div>
    </div>


    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function(){

         function clear_icon()
         {
          $('#nama_icon').html('');
         }

         function fetch_data(query)
         {
          $.ajax({
           url:"/get_ajax_data?query="+query,
           success:function(data)
           {
            $('#table_data').html('');
            $('#table_data').html(data);
           }
          })
         }

         $(document).on('keyup', '#serach', function(){
          var query = $('#serach').val();
          fetch_data(query);
         });

        });
    </script>

    <script>

        function produk() {
            $.get("{{ url('produk') }}", {}, function(data, status) {
                $("#table_data").html(data);
            });
        }
        function customer() {
            $.get("{{ url('customer') }}", {}, function(data, status) {
                $("#table_data").html(data);
            });
        }
        function agent() {
            $.get("{{ url('agent') }}", {}, function(data, status) {
                $("#table_data").html(data);
            });
        }
        function refresh() {
            $.get("{{ url('customer') }}", {}, function(data, status) {
                $("#table_data").html(data);
            });
        }
        // Untuk modal halaman edit show
        function show(id) {
            $.get("{{ url('show') }}/" + id, {}, function(data, status) {
                $("#exampleModalLabel").html('Detail Order')
                $("#page").html(data);
                $("#exampleModal").modal('show');
            });
        }
    </script>
</body>

</html>
