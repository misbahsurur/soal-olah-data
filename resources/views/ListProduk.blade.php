<div class="row">
	<div class="col-lg-12">
        <h4>list top 10 produk paling banyak terjual</h4>
		<table class="table table-bordered" id="laravel">
		   <thead>
			  <tr>
                <th>No</th>
                <th>Nama Produk</th>
                <th>Total Quantity</th>
                <th>Total Order</th>
			  </tr>
		   </thead>
		   <tbody>
				@if(!empty($data) && $data->count())
                @php
                    $no = 0;
                @endphp
                @foreach ($data as $item)
                @php
                    $no++;
                @endphp
				  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->product_name }}</td>
                    <td>{{ $item->sum }}</td>
                    <td>{{ $item->count }}</td>
				  </tr>
				  @endforeach
				@else
				<tr>
					<td colspan="4">No data found.</td>
				</tr>
				@endif
		   </tbody>
		</table>
        <a href="/">Kembali</a>
	</div>
</div>
