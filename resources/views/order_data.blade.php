<div class="row">
	<div class="col-lg-12">

		<table class="table table-bordered" id="laravel">
		   <thead>
			  <tr>
                <th>No</th>
                <th>Invoice</th>
                <th>Nama</th>
                <th>Address</th>
                <th>Status</th>
                <th>Aksi</th>
			  </tr>
		   </thead>
		   <tbody>
				@if(!empty($data) && $data->count())
                @php
                    $no = 0;
                @endphp
                @foreach ($data as $item)
                @php
                    $no++;
                @endphp
				  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->invoice_id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->address }}</td>
                    <td>
                        @if($item->status == 1)
                            new order
                        @elseif($item->status == 2)
                            payment success
                        @elseif($item->status == 3)
                            order process
                        @elseif($item->status == 4)
                            order completed
                        @elseif($item->status == 5)
                            order cancel
                        @elseif($item->status == 6)
                            payment pending
                        @elseif($item->status == 7)
                            payment failed
                        @endif
                    </td>
                    <td><button class="btn btn-info" onClick="show({{ $item->id }})">Detail</button></td>
				  </tr>
				  @endforeach
				@else
				<tr>
					<td colspan="4">No data found.</td>
				</tr>
				@endif
		   </tbody>
		</table>
	</div>
</div>
