<div class="row">
	<div class="col-lg-12">
        <h4>list top 10 agent paling banyak mendapatkan customer</h4>
		<table class="table table-bordered" id="laravel">
		   <thead>
			  <tr>
                <th>No</th>
                <th>Nama Agen</th>
                <th>Total Order</th>
			  </tr>
		   </thead>
		   <tbody>
				@if(!empty($data) && $data->count())
                @php
                    $no = 0;
                @endphp
                @foreach ($data as $item)
                @php
                    $no++;
                @endphp
				  <tr>
                    <td>{{ $no }}</td>
                    <td>{{ $item->store_name }}</td>
                    <td>{{ $item->count }}</td>
				  </tr>
				  @endforeach
				@else
				<tr>
					<td colspan="4">No data found.</td>
				</tr>
				@endif
		   </tbody>
		</table>
        <a href="/">Kembali</a>
	</div>
</div>
