<?php

namespace App\Http\Controllers;

use App\Order;
use App\Order_detail;
use Illuminate\Http\Request;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Order::all();
        return view('index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ListProduk()
    {
        $data = Order_detail::join('product','product.id','=','order_detail.product_id')
                ->select('product_id','product.product_name')
                ->selectRaw('SUM(qty) AS sum')
                ->selectRaw('COUNT(*) AS count')
                ->groupBy('product_id','product.product_name')
                ->orderByDESC('sum')
                ->limit(10)
                ->get();
        return view('ListProduk',compact('data'));
    }

    public function ListCustomer()
    {
        $data = Order::select('name')
                ->selectRaw('COUNT(*) AS count')
                ->groupBy('name')
                ->orderByDESC('count')
                ->limit(10)
                ->get();
        return view('ListCustomer',compact('data'));
    }

    public function ListAgent()
    {
        $data = Order::join('agent','agent.id','=','orders.agent_id')
                ->select('agent_id','agent.store_name')
                ->selectRaw('COUNT(*) AS count')
                ->groupBy('agent_id','agent.store_name')
                ->orderByDESC('count')
                ->limit(10)
                ->get();
        return view('ListAgent',compact('data'));
    }

    function get_ajax_data(Request $request)
    {
        if($request->ajax())
        {
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $data = Order::where('name', 'like', '%'.$query.'%')
                    ->orWhere('invoice_id', 'like', '%'.$query.'%')
                    ->orderBy('name', 'ASC')
                    ->get();

        return view('order_data', compact('data'))->render();
        }
    }

    function daterange(Request $request)
    {
        if($request->from_date != '' && $request->to_date != '')
            {
                $data = Order::whereBetween('delivery_date', array($request->from_date, $request->to_date))->orderBy('delivery_date', 'ASC')->get();
            } else {
                $data = Order::all();
            }
            return view('index', compact('data'))->render();
    }

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order_detail::join('orders','orders.id','=','order_detail.order_id')
                ->join('product','product.id','=','order_detail.product_id')
                ->where('order_id',$id)
                ->select('order_detail.*','orders.name','product.product_name')->first();
        return view('detail')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
